<?php
/*
 * 打印数组
 * $param array $array
 */
function p($array){
	dump($array,1,'',0);
}

/**
 * 检测用户是否登录
 * @return integer 0-未登录，大于0-当前登录用户ID
 */
function is_login(){
    $user = session('admin');
    if (empty($user)) {
        return 0;
    } else {
        return $user['id'];
    }
}

// 检测输入的验证码是否正确，$code为用户输入的验证码字符串
function check_verify($code, $id = ''){
    $verify = new \Think\Verify();
    return $verify->check($code, $id);
}

/**
 * 抽签程序获取球队名称
 */
function getTeamName($teamid){
	$TeamName="无名球队";
	$teamList = M('Team');
	$query = $teamList->where("id=".$teamid)->getField('tname',true);
	if($query){
		$TeamName=$query[0];
	}
	return $TeamName;
}
/**
 * 杯赛抽签方法
 */
function startCQ_Action($allnumber){
	if($allnumber == 32){
		//第一次抽签组建字符串cookie 记录抽签顺位
		for($i=1;$i<=32;$i++){
			$nlist[] = $i;
		}
		cookie('nlist',$nlist,28800);
	}
	else{
		$nlist=cookie("nlist");
	}
	//随机获取排位并将其在数组中清除
	$tempNum = array_splice($nlist,rand(0,$allnumber-1),1);
	cookie('nlist',$nlist,28800);
	return $tempNum[0];
}
/**
 * 预选赛抽签方法
 */
function startLCQ_Action($allnumber,$anum){
	if($allnumber == $anum){
		//第一次抽签组建字符串cookie 记录抽签顺位
		for($i=1;$i<=$anum;$i++){
			$nlist[] = $i;
		}
		cookie('l_nlist',$nlist,28800);
	}
	else{
		$nlist=cookie("l_nlist");
	}
	//随机获取排位并将其在数组中清除
	$tempNum = array_splice($nlist,rand(0,$allnumber-1),1);
	cookie('l_nlist',$nlist,28800);
	return $tempNum[0];
}
/**
 * 根据俱乐部ID获取俱乐部信息
 */
function getTeamInfo($tid,$colname='tname'){
	$teamList = M('Team');
	$tquery = $teamList->where("id=".$tid)->limit(1)->getField($colname,true);
	return $tquery[0];
}
/**
 * 根据小组ID获取小组信息
 */
function getGroupInfo($gid,$colname='name'){
	$Groups = M('Groups');
	$tquery = $Groups->where("id=".$gid)->limit(1)->getField($colname,true);
	return $tquery[0];
}
/**
 * 根据小组ID获取小组信息
 */
function getSeasonInfo($sid,$colname='name'){
	$seasonInfo = M('Season');
	$tquery = $seasonInfo->where("id=".$sid)->limit(1)->getField($colname,true);
	return $tquery[0];
}
/**
 * 获取当前赛季ID
 */
function getCurrentSeason($stype=0){
	$seasonInfo = M('Season');
	$tquery = $seasonInfo->where("is_current=1 and matchtype=".$stype)->order("id desc")->limit(1)->getField("id",true);
	return $tquery[0];
}
/**
 * 根据位置信息获取俱乐部信息
 */
function getPlayerNameByPos($gid,$pno,$mtype,$fname='player',$colname='tname'){
	$currentSeason = getCurrentSeason($mtype);
	if(is_numeric($currentSeason)){
		if($mtype==0){
			$matchInfo = M('League');
		}
		else
		{
			$matchInfo = M('Cup');
		}
		
		$tquery = $matchInfo->where("groupid=".$gid." and position=".$pno." and season=".$currentSeason)->limit(1)->getField($fname,true);
		if($fname=='player'){
			if($tquery){
				return getTeamInfo($tquery[0],$colname);
			}
		}
		else
		{
			if($tquery){
				return $tquery[0];
			}
			else
			{
				return 0;
			}
		}
		

	}
	return null;
}
function getPlayerNameByMatchPos($gid,$pno,$mtype,$fname='player',$colname='tname'){
	$currentSeason = getCurrentSeason($mtype);
	if(is_numeric($currentSeason)){
		$matchInfo = M('Match');
		$tquery = $matchInfo->where("groupid=".$gid." and matchtype=".$mtype." and position=".$pno." and season=".$currentSeason)->limit(1)->getField($fname,true);
		if($tquery){
			return getTeamInfo($tquery[0],$colname);
		}
	}
	return null;
}
?>