<?php
namespace Admin\Controller;
use Think\Controller;
class MatchController extends CommonController {
    public function ballot(){
    	$teamList = M('Team');
    	$query = $teamList->where("status=0")->select();
    	$this->assign('teamList',$query);
        $this->display();
    }
    public function clearballot(){
    	S('cqlist',null);
		S('teamno',null);
		S('cteam',null);
		S('l_cqlist',null);
		S('l_teamno',null);
		S('l_cteam',null);
		cookie(null);
		$this->success('操作成功，仪式缓存已清除',U('Match/ballot'));
    }
	public function cupballot($tid=0){
		if($tid == 0){
			//查询当前可报名的赛季
			$currentSeason = getCurrentSeason(1);
			if(is_numeric($currentSeason)){
				$s = $currentSeason;
				//对已报名队伍写入数组进行随机排序
				$teamList = M('Team');
				$matchInfo = M('Match');
				$subquery = $matchInfo->where("season=".$s." and matchtype=1")->getField('player',true);
				$map['status'] = 0;
				if($subquery)
				$map['id'] = array('not in',$subquery); //已经抽签完成的球队不会进入序列
	    		$query = $teamList->where($map)->getField('id',true);
	    		if(count($query) == 0){
	    			$this->error('已抽签完毕或没有球队报名！',U('Match/ballot'));
	    		}
	    		shuffle($query);
				cookie('cqlist',$query,28800); // 指定cookie保存时间 保存抽签列表
				cookie('season',$s,28800); // 保存当前赛季ID
				//定义内存存储机制
				S(array(
				    'type'=>'memcache',
				    'host'=>'127.0.0.1',
				    'port'=>'11211',
				    'expire'=>3600)
				);
				S('cqlist',null);
				S('teamno',null);
				S('cteam',null);
				S('cqlist',$query);
				//开始第一个球队的抽签
				$this->success('抽签60秒后开始...',U('Match/cupballot').'?tid='.$query[0],60);
			}
			else
			{
				$this->error('没有设定可报名的当前赛季，请返回设置后重试！',U('Match/ballot'));
			}
			return;
		}
		else
		{
			//单独进行显示并进入抽签程序方法
			$query = cookie('cqlist');
			$s = cookie('season');
			$Arraylength = count($query);
			if($Arraylength == 0){
    			$this->error('系统出错，请返回重试！',U('Match/ballot'));
    		}
    		$teamno=0;
    		S(array(
			    'type'=>'memcache',
			    'host'=>'127.0.0.1',
			    'port'=>'11211',
			    'expire'=>3600)
			);
    		$get_teamno_cache = S('teamno');
    		if($get_teamno_cache!=""){
    			$teamno = $get_teamno_cache;
    		}
    		$curTeam=$tid;
    		S('cteam',$curTeam);
			S('teamno',($teamno+1));

			$get_teamno_cache = S('teamno');
			$ret_text = "<h3>第".$get_teamno_cache."个正在参加抽签的球队是：".getTeamName($curTeam);
			$TeamPostion = startCQ_Action(32-$get_teamno_cache+1); //抽签获取顺位
			$ret_text.="<br />该球队抽到的顺位为".$TeamPostion;
			//var_dump($ret_text);
			//获取球队组别
			$groups = M('groups');
			$map['type'] = 1;
			$groups_query = $groups->where($map)->order('name asc')->field('id,name')->select();
			settype($TeamPostion,"int");
			$groupNo=(($TeamPostion-1) % 4 )+1;
			$groupId = floor(($TeamPostion-1)/4);
			settype($groupId,"int");
			$groupName=$groups_query[$groupId]['name'];

			$ret_text.="<br />将被分配到".$groupName."的".$groupNo."号位置！";

			//将抽签结果写入数据库
			$mdata['player'] = $tid;
			$mdata['season'] = $s;
			$mdata['matchtype'] = 1;
			$mdata['groupid'] = $groups_query[$groupId]['id'];
			$mdata['position'] = $groupNo; //顺位指的是当前小组的排位
			M('Match')->add($mdata);

			if(($Arraylength-1)<$get_teamno_cache){
				//抽签完毕 将抽签结果导入排名表
				$matchdata = M('Match')->where("season=".$s." and matchtype=1")->field('player,season,groupid,position')->select();
    			M('cup')->addAll($matchdata);
				$ret_text.="<br />抽签完毕！即将返回俱乐部列表页！</h1>";
				$ret_text.="<script>setTimeout(\"location.href=\'ballot.html\';\",15000);</script>";
			}
			else
			{
				$ret_text.="<br />15秒后进行下一支球队的抽签</h3>";
				$ret_text.="<a href=\"?tid=".$query[$get_teamno_cache]."\">点击继续</a>";
				$ret_text.="<script>setTimeout(\"location.href=\'?tid=".$query[$get_teamno_cache]."\';\",15000);</script>";
			}
			$this->assign('ret_text',$ret_text);
		}
        $this->display();
    }

    public function leagueballot($tid=0){
		if($tid == 0){
			//查询当前可报名的赛季
			$currentSeason = getCurrentSeason(0);
			if(is_numeric($currentSeason)){
				$s = $currentSeason;
				//对已报名队伍写入数组进行随机排序
				$teamList = M('Team');
				$matchInfo = M('Match');
				$subquery = $matchInfo->where("season=".$s." and matchtype=0")->getField('player',true);
				$map['status'] = 0;
				if($subquery)
				$map['id'] = array('not in',$subquery); //已经抽签完成的球队不会进入序列
	    		$query = $teamList->where($map)->getField('id',true);
	    		if(count($query) == 0){
	    			$this->error('已抽签完毕或没有球队报名！',U('Match/ballot'));
	    		}
	    		shuffle($query);
				cookie('l_cqlist',$query,28800); // 指定cookie保存时间 保存抽签列表
				cookie('l_season',$s,28800); // 保存当前赛季ID
				//定义内存存储机制
				S('l_cqlist',null);
				S('l_teamno',null);
				S('l_cteam',null);
				S('l_cqlist',$query);
				//开始第一个球队的抽签
				$this->success('抽签60秒后开始...',U('Match/leagueballot').'?tid='.$query[0],60);
			}
			else
			{
				$this->error('没有设定可报名的当前赛季，请返回设置后重试！',U('Match/ballot'));
			}
			return;
		}
		else
		{
			//单独进行显示并进入抽签程序方法
			$query = cookie('l_cqlist');
			$s = cookie('l_season');
			$Arraylength = count($query);
			if($Arraylength == 0){
    			$this->error('系统出错，请返回重试！',U('Match/ballot'));
    		}
    		$teamno=0;
    		$get_teamno_cache = S('l_teamno');
    		if($get_teamno_cache!=""){
    			$teamno = $get_teamno_cache;
    		}
    		$curTeam=$tid;
    		S('l_cteam',$curTeam);
			S('l_teamno',($teamno+1));

			$get_teamno_cache = S('l_teamno');
			$ret_text = "<h3>第".$get_teamno_cache."个正在参加抽签的球队是：".getTeamName($curTeam);
			$TeamPostion = startLCQ_Action($Arraylength-$get_teamno_cache+1,$Arraylength); //抽签获取顺位
			$ret_text.="<br />该球队抽到的顺位为".$TeamPostion;
			//var_dump($ret_text);
			//获取球队组别
			
			settype($TeamPostion,"int");
			$gnum = 20; //初始化预选赛每个小组的分组球队数
	    	$gsnum = 1; //需要分多少组?

	    	if(floor($Arraylength/$gnum)>=1){
	    		//得分组
	    		if(fmod($Arraylength,$gnum)>0){
	    			//除了之后加一为除数
	    			$gsnum = floor($Arraylength/$gnum)+1;
	    		}
	    		else
	    		{
	    			if(floor($Arraylength/$gnum)>1){
	    				//除了之后加一为除数
	    				$gsnum = floor($Arraylength/$gnum);
	    			}
	    			
	    		}
	    	}
			$groups = M('groups');
			$gmap['type'] = 0;
			$gmap['level'] = 0;
			$groups_tmp_query = $groups->where($gmap)->order('id asc')->getField('id',true);
			$minGroupId = $groups_tmp_query[0];
			settype($minGroupId,"int");
			for ($i=0; $i < $gsnum; $i++) { 
				$data['id'] = $minGroupId+$i;
				if($gsnum==1){
					$data['name'] = '预选赛';
				}
				else
				{
					$data['name'] = '预选赛'.($i+1).'区';
				}
				
				$data['type'] = 0;
				$data['level'] = 0;
				$groups->add($data,array(),true);
			}

			$gnum = $Arraylength / $gsnum;
			$groupName='';
	    	for($i=1;$i<=fmod($Arraylength,$gsnum);$i++){
	    		$gnum = floor($Arraylength / $gsnum)+1;
	    		if($gnum*$i >= $TeamPostion){
	    			$groupNo=(($TeamPostion-1) % $gnum )+1;
	    			$groupId = $minGroupId+floor(($TeamPostion-1)/$gnum);
	    			$groupName='预选赛'.$i.'区';
	    			break;
	    		}
			}
			if($groupName==''){
				for($i=fmod($Arraylength,$gsnum);$i<=$gsnum;$i++){
					$gnum = floor($Arraylength / $gsnum);
					if($gnum*$i >= $TeamPostion){
		    			$groupNo=(($TeamPostion-1) % $gnum )+1;
		    			$groupId = $minGroupId+floor(($TeamPostion-1)/$gnum);
		    			$groupName='预选赛'.$i.'区';
		    			break;
		    		}
				}
			}
			settype($groupId,"int");
			if($gsnum==1){
				$groupName='预选赛';
			}

			$ret_text.="<br />将被分配到".$groupName."的".$groupNo."号位置！";

			//将抽签结果写入数据库
			$mdata['player'] = $tid;
			$mdata['season'] = $s;
			$mdata['matchtype'] = 0;
			$mdata['groupid'] = $groupId;
			$mdata['position'] = $groupNo; //顺位指的是当前小组的排位
			M('Match')->add($mdata);

			if(($Arraylength-1)<$get_teamno_cache){
				//抽签完毕 将抽签结果导入排名表
				$matchdata = M('Match')->where("season=".$s." and matchtype=0")->field('player,season,groupid,position')->select();
    			M('league')->addAll($matchdata);
				$ret_text.="<br />抽签完毕！即将返回俱乐部列表页！</h1>";
				$ret_text.="<script>setTimeout(\"location.href=\'ballot.html\';\",15000);</script>";
			}
			else
			{
				$ret_text.="<br />15秒后进行下一支球队的抽签</h3>";
				$ret_text.="<a href=\"?tid=".$query[$get_teamno_cache]."\">点击继续</a>";
				$ret_text.="<script>setTimeout(\"location.href=\'?tid=".$query[$get_teamno_cache]."\';\",15000);</script>";
			}
			$this->assign('ret_text',$ret_text);
		}
        $this->display();
    }

}