<?php
namespace Admin\Controller;
class PublicController extends CommonController {
    public function top(){
        $this->display();
    }
    public function sidebar(){
    	$current_path = str_replace(__MODULE__.'/','',__ACTION__);
    	$current_action = str_replace(__MODULE__.'/','',__CONTROLLER__);
        $this->display();
    }   
}