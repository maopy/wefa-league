<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head lang="zh-cn">
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="renderer" content="webkit">
	<title>WEFA联盟</title>
	<link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<!--[if lt IE 9]>
	<script src="http://cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="http://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

	<style>
		.team-list-wrapper button {
			margin: 0 0 10px 0;
		}
	</style>

</head>
<body>
<nav class="navbar navbar-default" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<a class="navbar-brand" href="#">WEFA联盟</a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li class="active"><a href="\Home\Index\index">主页</a></li>
				<li><a href="\League\Index\index">联赛</a></li>
				<li><a href="\League\Cup\index">杯赛</a></li>
			</ul>
			<div class="nav navbar-nav navbar-right">
				<a type="button" class="btn btn-default navbar-btn" href="\Team\Index\signUp">注册俱乐部</a>
				<a type="button" class="btn btn-default navbar-btn" href="\Team\Index\login">登录</a>
			</div>
		</div><!--/.nav-collapse -->
	</div>
</nav>
<div class="container">
	<div class="jumbotron">
		<h1>I want u !</h1>
		<p>第二届WEFA联盟联赛将同时进行联赛和杯赛，现已开启报名！</p>
		<p><a href="\Team\Index\signUp" class="btn btn-primary btn-lg" role="button">报名</a></p>
	</div>
	<?php if($teamList == false): else: ?>
	<div class="jumbotron span12">
		<h5>已注册俱乐部：</h5>
		<p class="team-list-wrapper">
			<?php if(is_array($teamList)): $i = 0; $__LIST__ = $teamList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$data): $mod = ($i % 2 );++$i;?><button type="button" class="btn btn-info btn-sm"><?php echo ($data["tname"]); ?></button> &nbsp;<?php endforeach; endif; else: echo "" ;endif; ?>
		</p>
	</div><?php endif; ?>
</div>

<script src="http://cdn.bootcss.com/jquery/2.1.1/jquery.min.js"></script>
<script src="http://cdn.bootcss.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script>
	$(function () {

	});
</script>
</body>
</html>