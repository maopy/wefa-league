<?php
namespace League\Controller;
use Think\Controller;
use Common\Common\JSONHelper as JSONHelper;
class BallotController extends Controller {
	public function index(){
//		$this->show('<style type="text/css">*{ padding: 0; margin: 0; } div{ padding: 4px 48px;} body{ background: #fff; font-family: "微软雅黑"; color: #333;font-size:24px} h1{ font-size: 100px; font-weight: normal; margin-bottom: 12px; } p{ line-height: 1.8em; font-size: 36px }</style><div style="padding: 24px 48px;"> <h1>:)</h1><p>欢迎使用 <b>ThinkPHP</b>！</p><br/>[ 您现在访问的是Home模块的Index控制器 ]</div><script type="text/javascript" src="http://tajs.qq.com/stats?sId=9347272" charset="UTF-8"></script>','utf-8');
	}
	public function cup() {
		$currentSeason = getCurrentSeason(1);
		if(is_numeric($currentSeason)){
			$teamList = M('Team');
			$matchInfo = M('Match');
			$subquery = $matchInfo->where("season=".$currentSeason)->getField('player',true);
			$map['status'] = 0;
			if($subquery)
			$map['id'] = array('not in',$subquery); //已经抽签完成的球队不会进入序列
			$query = $teamList->where($map)->getField('id',true);
			if(count($query) == 0){
				$this->error('你来晚啦！'.getSeasonInfo($currentSeason).'的抽签仪式已结束，请直接到相关杯赛页面查看球队所处分组！',U('/League/Cup/index')); 
			}
			S(array(
			    'type'=>'memcache',
			    'host'=>'127.0.0.1',
			    'port'=>'11211',
			    'expire'=>3600)
			);
			$cteam = S('cteam');
			$cqlist = S('cqlist');
			if($cteam){
				$this->assign('secnum','10');
			}
			else
			{
				$this->assign('secnum','450');
			}
			$this->assign('tnum',count($cqlist));
			//获取杯赛分组
			$groupList = M('Groups');
			$groupquery = $groupList->where("type=1")->order("name asc")->field("id,name")->select();
			$this->assign('glist',$groupquery);
		}
		else
		{
			$this->error('赛事数据还未设置，尽请期待哦！',U('/Home/index'));
		}
		$this->display();
	}
	public function league() {
		$currentSeason = getCurrentSeason(0);
		if(is_numeric($currentSeason)){
			$teamList = M('Team');
			$matchInfo = M('Match');
			$subquery = $matchInfo->where("season=".$currentSeason)->getField('player',true);
			$map['status'] = 0;
			if($subquery)
			$map['id'] = array('not in',$subquery); //已经抽签完成的球队不会进入序列
			$query = $teamList->where($map)->getField('id',true);
			if(count($query) == 0){
				$this->error('你来晚啦！'.getSeasonInfo($currentSeason).'的抽签仪式已结束，请直接到相关联赛页面查看球队所处赛区！',U('/League/Index/index')); 
			}
			S(array(
			    'type'=>'memcache',
			    'host'=>'127.0.0.1',
			    'port'=>'11211',
			    'expire'=>3600)
			);
			$cteam = S('l_cteam');
			$cqlist = S('l_cqlist');
			if($cteam){
				$this->assign('secnum','100');
			}
			else
			{
				$this->assign('secnum','600');
			}
			$this->assign('tnum',count($cqlist));
			//获取杯赛分组
			$groupList = M('Groups');
			$groupquery = $groupList->where("type=0")->order("name asc")->field("id,name")->select();
			$this->assign('glist',$groupquery);
		}
		else
		{
			$this->error('赛事数据还未设置，尽请期待哦！',U('/Home/index'));
		}
		$this->display();
	}
}