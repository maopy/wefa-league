<?php
namespace League\Controller;
use Think\Controller;
use Common\Common\JSONHelper as JSONHelper;
class CupController extends Controller {
	public function index(){
		$currentSeason = getCurrentSeason(1);
		$retext = "";
		if(is_numeric($currentSeason)){
			$teamList = M('Team');
			$matchInfo = M('Match');
			$subquery = $matchInfo->where("season=".$currentSeason)->getField('player',true);
			$map['status'] = 0;
			if($subquery)
			$map['id'] = array('not in',$subquery); //已经抽签完成的球队不会进入序列
			$query = $teamList->where($map)->getField('id',true);
			if(count($query) == 0){
				$retext = "";
			}
			else
			{
				$retext = "<p><a href=\"".__APP__."\League\Ballot\cup\" class=\"btn btn-primary btn-lg\" role=\"button\">抽签仪式</a></p>";
			}
		}
		$this->assign('retext',$retext);

		//获取杯赛分组
		$groupList = M('Groups');
		$groupquery = $groupList->where("type=1")->order("name asc")->field("id,name")->select();
		$this->assign('glist',$groupquery);
		$this->display();
	}
	
}