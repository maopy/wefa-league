<?php
namespace Home\Controller;
use Think\Controller;
class IndexController extends Controller {
    public function index(){
    	$teamList = M('Team');
    	$query = $teamList->select();
    	$this->assign('teamList',$query);
        $this->display();
    }
    public function cqaction($steps=0){
    	//第一步展示随机序列
    	if($steps == 0){
    		S(array(
			    'type'=>'memcache',
			    'host'=>'127.0.0.1',
			    'port'=>'11211',
			    'expire'=>3600)
			);
			$cqlist = S('cqlist');
			if(count($cqlist) == 0){
				return;
			}
			$cqlist_html="";

			foreach ($cqlist as $k=>$v) {
			    //遍历数组获取抽签列表的俱乐部名
			    if($cqlist_html != ""){
			    	$cqlist_html.="|&nbsp;<button type=\"button\" class=\"btn btn-info btn-sm\">".getTeamInfo($v)."</button>";
			    }
			    else{
			    	$cqlist_html="<button type=\"button\" class=\"btn btn-info btn-sm\">".getTeamInfo($v)."</button>";
			    }
			}
			echo $cqlist_html;
    	}
    	//第二步重组当前正在抽签的俱乐部信息
    	if($steps == 1){
    		S(array(
			    'type'=>'memcache',
			    'host'=>'127.0.0.1',
			    'port'=>'11211',
			    'expire'=>3600)
			);
			$cqlist = S('cqlist');
			if(count($cqlist) > 0){
				$curTeam = S('cteam');
				$teamno = S('teamno');
				if(is_numeric($curTeam) && is_numeric($teamno)){
					echo $teamno."|".$curTeam."|".getTeamInfo($curTeam);
				}
				else
				{
					echo "|";
				}
			}
    	}
    	//第三步获取当前抽签的俱乐部抽签结果
    	if($steps == 2){
    		S(array(
			    'type'=>'memcache',
			    'host'=>'127.0.0.1',
			    'port'=>'11211',
			    'expire'=>3600)
			);
			$curTeam = S('cteam');
			$curSeason = getCurrentSeason(1); //获取当前可报名赛季

			if(is_numeric($curTeam) && is_numeric($curSeason)){
				$matchInfo = M('Match');
				$mquery = $matchInfo->where("player=".$curTeam." and season=".$curSeason." and matchtype=1")->field('groupid,position')->limit(1)->select();
				if(count($mquery) >0){
					$groupName = getGroupInfo($mquery[0]['groupid']); 	//获取分组名称
					$position = $mquery[0]['position'];					//获取分组签位
					echo getTeamInfo($curTeam)."|".$groupName."的".$position."号|".$groupName;
				}
			}
			else{
				echo "|";
			}
    	}
    	$this->display();
    }
    public function lcqaction($steps=0){
    	//第一步展示随机序列
    	if($steps == 0){
			$lcqlist = S('l_cqlist');
			if(count($lcqlist) == 0){
				return;
			}
			$cqlist_html="";

			foreach ($lcqlist as $k=>$v) {
			    //遍历数组获取抽签列表的俱乐部名
			    if($cqlist_html != ""){
			    	$cqlist_html.="|&nbsp;<button type=\"button\" class=\"btn btn-info btn-sm\">".getTeamInfo($v)."</button>";
			    }
			    else{
			    	$cqlist_html="<button type=\"button\" class=\"btn btn-info btn-sm\">".getTeamInfo($v)."</button>";
			    }
			}
			echo $cqlist_html;
    	}
    	//第二步重组当前正在抽签的俱乐部信息
    	if($steps == 1){
			$cqlist = S('l_cqlist');
			if(count($cqlist) > 0){
				$curTeam = S('l_cteam');
				$teamno = S('l_teamno');
				if(is_numeric($curTeam) && is_numeric($teamno)){
					echo $teamno."|".$curTeam."|".getTeamInfo($curTeam);
				}
				else
				{
					echo "|";
				}
			}
    	}
    	//第三步获取当前抽签的俱乐部抽签结果
    	if($steps == 2){
			$curTeam = S('l_cteam');
			$curSeason = getCurrentSeason(0); //获取当前可报名赛季

			if(is_numeric($curTeam) && is_numeric($curSeason)){
				$matchInfo = M('Match');
				$mquery = $matchInfo->where("player=".$curTeam." and season=".$curSeason." and matchtype=0")->field('groupid,position')->limit(1)->select();
				if(count($mquery) >0){
					$groupName = getGroupInfo($mquery[0]['groupid']); 	//获取分组名称
					$position = $mquery[0]['position'];					//获取分组签位
					echo getTeamInfo($curTeam)."|".$groupName."的".$position."号|".$position;
				}
			}
			else{
				echo "|";
			}
    	}
    	$this->display();
    }

}